package com.besheater.training.javaconcurrency;

import com.besheater.training.javaconcurrency.thread.MainDiagonalFillerThread;
import com.besheater.training.javaconcurrency.matrix.ConcurrentMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

public class ConcurrentMatrixUpdater {
    private static final Logger LOG = LogManager.getLogger();

    private static final String CONFIG_FILE_PATH = "/matrix.properties";

    private int matrixDimension;
    private int threadsCount;
    private ConcurrentMatrix matrix;
    private List<Thread> threads;
    private CyclicBarrier startingBarrier;

    public static void main(String[] args) {
        try {
            new ConcurrentMatrixUpdater().start();
        } catch (Exception ex) {
            LOG.error("Application crashed", ex);
            ex.printStackTrace();
        }
    }

    public ConcurrentMatrixUpdater() throws IOException {
        ConfigData configData = getConfigData();
        matrixDimension = configData.getMatrixDimension();
        threadsCount = configData.getThreadsCount();
        matrix = new ConcurrentMatrix(matrixDimension, matrixDimension);
        threads = new ArrayList<>(threadsCount);
        startingBarrier = new CyclicBarrier(threadsCount);
        LOG.info("New instance created");
    }

    public void start() throws InterruptedException {
        System.out.println("Matrix dimension, N = " + matrixDimension);
        System.out.println("Threads count, M = " + threadsCount);
        System.out.println("Initial matrix:");
        System.out.println(matrix.prettyPrint());
        createThreads();
        startThreads();
        waitForAllThreadsToFinish();
        System.out.println("Final matrix:");
        System.out.println(matrix.prettyPrint());
    }

    private void createThreads() {
        for (int i = 1; i <= threadsCount; i++) {
            threads.add(new MainDiagonalFillerThread(matrix, i, startingBarrier));
        }
        LOG.info("{} threads created", threadsCount);
    }

    private void startThreads() {
        for (Thread thread : threads) {
            thread.start();
        }
        LOG.info("All threads started");
    }

    private void waitForAllThreadsToFinish() throws InterruptedException {
        LOG.info("Waiting for all threads to finish");
        for (Thread thread : threads) {
            thread.join();
        }
        LOG.info("All threads finished");
    }

    private ConfigData getConfigData() throws IOException {
        return new ConfigData(getClass().getResourceAsStream(CONFIG_FILE_PATH));
    }
}