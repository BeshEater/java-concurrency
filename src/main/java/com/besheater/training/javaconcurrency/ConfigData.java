package com.besheater.training.javaconcurrency;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigData {
    private static final Logger LOG = LogManager.getLogger();

    public static final int MIN_MATRIX_DIMENSION = 8;
    public static final int MAX_MATRIX_DIMENSION = 12;
    public static final int MIN_THREADS_COUNT = 4;
    public static final int MAX_THREADS_COUNT = 6;
    public static final String MATRIX_DIMENSION_PROPERTY_NAME = "matrix.dimension.N";
    public static final String THREAD_COUNT_PROPERTY_NAME = "thread.count.M";

    private final Properties configData;

    public ConfigData(InputStream configFileInputStream) throws IOException {
        configData = getConfigurationData(configFileInputStream);
        checkMatrixDimension(getMatrixDimension());
        checkThreadsCount(getThreadsCount());
        LOG.info("New instance created");
    }

    public int getMatrixDimension() {
        return Integer.parseInt(configData.getProperty(MATRIX_DIMENSION_PROPERTY_NAME));
    }

    public int getThreadsCount() {
        return Integer.parseInt(configData.getProperty(THREAD_COUNT_PROPERTY_NAME));
    }

    private void checkMatrixDimension(int matrixDimension) {
        if (matrixDimension < MIN_MATRIX_DIMENSION || matrixDimension > MAX_MATRIX_DIMENSION) {
            String message = String.format("Matrix dimension %d is not valid. " +
                            "Please provide integer value from %d to %d",
                             matrixDimension, MIN_MATRIX_DIMENSION, MAX_MATRIX_DIMENSION);
            throw new IllegalArgumentException(message);
        }
        LOG.debug("Matrix dimension {} is valid", matrixDimension);
    }

    private void checkThreadsCount(int threadsCount) {
        if (threadsCount < MIN_THREADS_COUNT || threadsCount > MAX_THREADS_COUNT) {
            String message = String.format("Threads count %d is not valid. " +
                            "Please provide integer value from %d to %d",
                             threadsCount, MIN_THREADS_COUNT, MAX_THREADS_COUNT);
            throw new IllegalArgumentException(message);
        }
        LOG.debug("Threads count {} is valid", threadsCount);
    }

    private Properties getConfigurationData(InputStream configFileInputStream) throws IOException {
        Properties configData = new Properties();
        configData.load(configFileInputStream);
        return configData;
    }
}