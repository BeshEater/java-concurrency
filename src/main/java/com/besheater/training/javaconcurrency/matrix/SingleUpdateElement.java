package com.besheater.training.javaconcurrency.matrix;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SingleUpdateElement {
    private static final Logger LOG = LogManager.getLogger();

    private int value;
    private boolean isUpdated;
    private final Lock updateLock = new ReentrantLock();

    public SingleUpdateElement(int initialValue) {
        value = initialValue;
        isUpdated = false;
        LOG.info("New instance created");
    }

    public int getValue() {
        return value;
    }

    public boolean update(int newValue) {
        if (isUpdated) {
            return false;
        }
        if (updateLock.tryLock()) {
            try {
                value = newValue;
                isUpdated = true;
                return true;
            } finally {
                updateLock.unlock();
            }
        } else {
            // Somebody already updating value
            return false;
        }
    }

    @Override
    public String toString() {
        return "SingleUpdateElement{" +
                "value=" + value +
                ", isUpdated=" + isUpdated +
                '}';
    }
}