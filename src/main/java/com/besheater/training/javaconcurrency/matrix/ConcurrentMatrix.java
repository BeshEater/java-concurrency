package com.besheater.training.javaconcurrency.matrix;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConcurrentMatrix {
    private static final Logger LOG = LogManager.getLogger();

    private static final int INITIAL_ENTRIES_VALUE = 0;

    private final SingleUpdateElement[][] data;

    public ConcurrentMatrix(int rowDimension, int columnDimension) {
        data = new SingleUpdateElement[rowDimension][columnDimension];
        for (int rowIndex = 0; rowIndex < rowDimension; rowIndex++) {
            for (int columnIndex = 0; columnIndex < columnDimension; columnIndex++) {
                data[rowIndex][columnIndex] = new SingleUpdateElement(INITIAL_ENTRIES_VALUE);
            }
        }
        LOG.info("Matrix with {} rows and {} columns created", rowDimension, columnDimension);
    }

    public void fillMainDiagonal(int value) {
        int rowsCount = data.length;
        int columnsCount = data[0].length;
        LOG.debug("Start updating diagonal entries");
        for (int i = 0; i < rowsCount && i < columnsCount; i++) {
            boolean isUpdated = data[i][i].update(value);
            if (isUpdated) {
                LOG.debug("Entry[{}, {}] updated value = {}", i, i, data[i][i].getValue());
            } else {
                LOG.debug("Entry[{}, {}] is already updated", i, i);
            }
        }
        LOG.debug("Finished updating diagonal entries");
    }

    public String prettyPrint() {
        int entryWidth = getLongestNumberLength() + 1;
        String rowPrefix = "[ ";
        String rowSuffix = " ]";
        String rowSeparator = "\n";
        String columnSeparator = " | ";
        String entryFormatPattern = "%" + entryWidth + "d";
        StringBuilder builder = new StringBuilder();
        for (int rowIndex = 0; rowIndex < data.length; rowIndex++) {
            builder.append(rowPrefix);
            SingleUpdateElement[] row = data[rowIndex];
            for (int columnIndex = 0; columnIndex < row.length; columnIndex++) {
                int entryValue = row[columnIndex].getValue();
                builder.append(String.format(entryFormatPattern, entryValue));
                if (columnIndex != row.length - 1) {
                    builder.append(columnSeparator);
                }
            }
            builder.append(rowSuffix);
            builder.append(rowSeparator);
        }
        return builder.toString();
    }

    private int getLongestNumberLength() {
        int longestLength = 0;
        for (int rowIndex = 0; rowIndex < data.length; rowIndex++) {
            SingleUpdateElement[] row = data[rowIndex];
            for (int columnIndex = 0; columnIndex < row.length; columnIndex++) {
                int entryValue = row[columnIndex].getValue();
                int entryValueLength = getNumberLength(entryValue);
                if (entryValueLength > longestLength) {
                    longestLength = entryValueLength;
                }
            }
        }
        LOG.trace("Longest number length = {}", longestLength);
        return longestLength;
    }

    private int getNumberLength(int number) {
        return Integer.toString(Math.abs(number)).length();
    }
}