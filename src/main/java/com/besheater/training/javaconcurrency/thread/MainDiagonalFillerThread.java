package com.besheater.training.javaconcurrency.thread;

import com.besheater.training.javaconcurrency.matrix.ConcurrentMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class MainDiagonalFillerThread extends Thread {
    private static final Logger LOG = LogManager.getLogger();

    private final ConcurrentMatrix matrix;
    private final int value;
    private final CyclicBarrier startingBarrier;

    public MainDiagonalFillerThread(ConcurrentMatrix matrix, int value, CyclicBarrier startingBarrier) {
        this.matrix = matrix;
        this.value = value;
        this.startingBarrier = startingBarrier;
        LOG.info("New instance created");
    }

    @Override
    public void run() {
        try {
            LOG.debug("Awaits on starting barrier");
            startingBarrier.await();
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            LOG.error("Awaits on starting barrier interrupted", ex);
            throw new RuntimeException(ex);
        } catch (BrokenBarrierException ex) {
            LOG.error("Starting barrier is already broken", ex);
            throw new RuntimeException(ex);
        }
        LOG.debug("Start updating entries");
        matrix.fillMainDiagonal(value);
        LOG.debug("Finished updating entries");
    }
}